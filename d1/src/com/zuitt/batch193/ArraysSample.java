package com.zuitt.batch193;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class ArraysSample {

    public static void main(String[] args) {
        //Arrays = list of things. In JS, we can have as many as we want. But in Java, it is LIMITED.

        //Array Declaration/Instantiation
        int[] intArray = new int[3];
        //int[] - indicate that this is an int datatype and can hold multiple int values
        //new keyword tells Java to create the said identifier
        //int[3] - indicates the amount of integer values in the array

        intArray[0] = 10;
        intArray[1] = 20;
        intArray[2] = 30;
        //intArray[3] = 30; Index 3 out of bounds for length 3
        System.out.println(intArray[2]);

        //other way to declare an array
        int[] intSample = new int[3];
        intSample[0] = 50;
        System.out.println(intSample[0]);

        //String array
        String[] stringArray = new String[3];
        stringArray[0] = "Anya";
        stringArray[1] = "Loid";
        stringArray[2] = "Yor";
        System.out.println("The Forger family: " + stringArray[1] + ", " + stringArray[0] + " and " + stringArray[2]);

        //Declaration with initialization based on number of elements
        int[] intArray2 = {100, 200, 300, 400, 500};
        System.out.println(intArray2[0]);

        //System.out.println(intArray2);//it will print out the memory allocation of the array

        //to get the actual value of array
        System.out.println(Arrays.toString(intArray2));
        //Arrays is a class contains various methods for manipulating arrays
        //.toString() is a method that returns a string representation of the contents of the specified array.

        //methods used in arrays
        Arrays.sort(stringArray);
        System.out.println(Arrays.toString(stringArray));

        //binarySearch
        String searchTerm = "Aaa";
        int binaryResult = Arrays.binarySearch(stringArray, searchTerm);
        System.out.println(binaryResult);
        //binarySearch will return an index if the match is found

        //Multidimensional array
        //two-dimensional array, can be described by two lengths nested each other like a matrix
        String[][] classroom = new String[3][3];

        //first row
        classroom[0][0] = "Dahyun";
        classroom[0][1] = "Chaeyoung";
        classroom[0][2] = "Nayeon";
        //second row
        classroom[1][0] = "Luffy";
        classroom[1][1] = "Zoro";
        classroom[1][2] = "Sanji";
        //third row
        classroom[2][0] = "Loid";
        classroom[2][1] = "Yor";
        classroom[2][2] = "Anya";

        //.deepToString() returns a string of the nested/deep contents of the array
        System.out.println(Arrays.deepToString(classroom));

        //ArrayList
        //is a resizeable-array. Implements all optional operations, and permits all elements and can be added or removed whenever it is needed.
        ArrayList<String> students = new ArrayList<>();

        //adding elements to an ArrayList is done by using the add()
        students.add("John");
        students.add("Paul");
        System.out.println(students);

        //retrieving elements using get()
        System.out.println(students.get(0));

        //update or changing elements
        students.set(1, "George"); //first value -> index, second value -> updatedValue
        System.out.println(students);

        //delete or remove elements
        students.remove(1); //index number
        System.out.println(students);

        //removing ALL elements in an ArrayList
        students.clear(); //clear all the list of the elements
        System.out.println(students);

        //get length of an ArrayList
        System.out.println(students.size());

        //ArrayList with initial values
        ArrayList<String> employees = new ArrayList<>(Arrays.asList("June", "Albert"));
        System.out.println(employees);


        //HashMaps
        //are stored by specifying the data type of the dkey and of the value
        //flexible object without pattern
        //HashMap<fieldDataType, valueDataType>
        HashMap<String, String> employeeRole = new HashMap<>();

        //adding elements to HashMaps
        employeeRole.put("Captain", "Luffy");
        employeeRole.put("Doctor", "Chopper");
        employeeRole.put("Navigator", "Nami");

        System.out.println(employeeRole);

        //retrieve the value using the field
        System.out.println(employeeRole.get("Captain"));

        //remove fields
        employeeRole.remove("Doctor");
        System.out.println(employeeRole);

        //getting the keys of the elements of HashMaps by using the keySet()
        System.out.println(employeeRole.keySet());

        //HashMaps with int as values
        HashMap<String, Integer> grades = new HashMap<>();

        grades.put("Joe", 89);
        grades.put("John", 93);
        System.out.println(grades);

        ////HashMap with ArrayList
        HashMap<String, ArrayList<Integer>> subjectGrades = new HashMap<>();
        ArrayList<Integer> gradeListA = new ArrayList<>(Arrays.asList(80, 75, 90));
        ArrayList<Integer> gradeListB = new ArrayList<>(Arrays.asList(86, 87, 96));

        subjectGrades.put("Joe", gradeListA);
        subjectGrades.put("John", gradeListB);

        System.out.println(subjectGrades);

        //get a specific value
        System.out.println(subjectGrades.get("Joe").get(2));
    }
}