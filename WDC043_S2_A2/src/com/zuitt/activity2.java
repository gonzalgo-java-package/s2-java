package com.zuitt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class activity2 {

    public static void main(String[] args) {

        //Array
        int[] primeNumbers = {2, 3, 5, 7, 11};
        System.out.println("The first prime number is: " + primeNumbers[0]);

        //ArrayList
        ArrayList<String> names = new ArrayList<>();
        names.add("John");
        names.add("Jane");
        names.add("Chloe");
        names.add("Zoey");
        System.out.println("My friends are: " + names);

        //HashMap
        HashMap<String, Integer> inventory = new HashMap<>();
        inventory.put("toothpaste", 15);
        inventory.put("toothbrush", 20);
        inventory.put("soap", 12);
        System.out.println("Our current inventory consists of: " + inventory);
    }
}
